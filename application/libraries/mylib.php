<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mylib {
    private $CI;
    public function __construct(){
        $this->CI = & get_instance();
    }
    public function view($view,$content=array()){

        $this->CI->load->view('header');
        $this->CI->load->view($view,$content);
        $this->CI->load->view('footer');
    }
}
?>