<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        if(!$this->session->userdata('admin_logged')){
            redirect(site_url('Admin_login'));
        }
    }

	public function index(){
       $data['posts'] = $this->posts_model->get_all_posts();
	   $this->mylib->view('admin_posts',$data);
    }
    
    public function edit_post($post_id = 0){
        $post_id = abs((int)$post_id);
        $this->load->helper('form');
	    $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'title', 'required|max_length[255]|htmlspecialchars|xss_clean');
        $this->form_validation->set_rules('description', 'description', 'required|max_length[255]|htmlspecialchars|xss_clean');
        $this->form_validation->set_rules('text', 'text', 'required|xss_clean');
        if ($this->form_validation->run()){
            $this->posts_model->update_post($post_id);
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['post'] = $this->posts_model->get_post($post_id);
        if(!$data['post']){
            show_404();
        }
        $this->mylib->view('admin_post',$data);
    }
    
    public function add_post(){
        $this->load->helper('form');
	    $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'title', 'required|max_length[255]|htmlspecialchars|xss_clean');
        $this->form_validation->set_rules('description', 'description', 'required|max_length[255]|htmlspecialchars|xss_clean');
        $this->form_validation->set_rules('text', 'text', 'required|xss_clean');
        if ($this->form_validation->run()){
            $id = $this->posts_model->add_post();
            redirect(site_url('posts/view_post/'.$id));
        }
        $data['post'] = false;
        $this->mylib->view('admin_post',$data);
    }
    
    public function delete_post($post_id = 0){
        $post_id = abs((int)$post_id);
        $this->posts_model->delete_post($post_id);
        redirect($_SERVER['HTTP_REFERER']);
    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */