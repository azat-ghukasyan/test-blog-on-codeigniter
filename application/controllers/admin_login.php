<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_login extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
    }

	public function index(){
	   if($this->session->userdata('admin_logged')){
            redirect(site_url('admin'));
       }
	   $this->load->helper('form');
	   $this->load->library('form_validation');
       $this->load->model('login_model');
	   $this->form_validation->set_rules('username', 'Login', 'required');
	   $this->form_validation->set_rules('password', 'Password', 'required|sha1');
       if ($this->form_validation->run()){
            if($this->login_model->get_auth($this->input->post('username'),$this->input->post('password'))){
                $this->session->set_userdata('admin_logged',true);
                redirect(site_url('admin'));  
			}
            else{
                $this->session->set_flashdata('error',true);
                redirect($_SERVER['HTTP_REFERER']);
            }
	   }
       $this->mylib->view('login');
    }
    
    public function logout(){
        $this->session->sess_destroy();;
        redirect($_SERVER['HTTP_REFERER']);
    }
    

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */