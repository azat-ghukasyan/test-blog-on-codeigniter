<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Posts extends CI_Controller {


	public function index()
	{
	   $data['posts'] = $this->posts_model->get_all_posts();
	   $this->mylib->view('posts',$data);
	}
    
    public function view_post($post_id = 0){
        $post_id = abs((int)$post_id);
        $data['post'] = $this->posts_model->get_post($post_id);
        if(!$data['post']){
            show_404();
        }
        $this->mylib->view('post',$data);
    }
}

/* End of file posts.php */
/* Location: ./application/controllers/posts.php */