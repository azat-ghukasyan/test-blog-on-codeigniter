<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Posts_model extends CI_Model {
    
    public function get_all_posts(){
        $this->db->select('id,title,description,date');
        $this->db->order_by('id DESC');
        $query = $this->db->get('posts');
        if($query->num_rows()>0){
            return $query->result_array();
        }
    }
    
    public function get_post($post_id){
        $query = $this->db->get_where('posts',array('id'=>$post_id));
        if($query->num_rows()>0){
            return $query->row_array();
        }
        else return false;
    }
    
    public function delete_post($post_id){
        $this->db->where('id',$post_id);
        $this->db->delete('posts');
    }
    
    public function update_post($post_id){
        $this->db->where('id',$post_id);
        $this->db->update('posts',$_POST);
    }
    
    public function add_post(){
        $this->db->insert('posts',$_POST);
        return $this->db->insert_id();
    }

}
?>