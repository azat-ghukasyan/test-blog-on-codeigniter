<h1>Posts
<a href="<?php echo site_url('admin/add_post');?>">Add Post</a>
</h1>

<?php if($posts) foreach($posts as $post):?>
<div class="posts">
    <h3><?php echo $post['title'];?></h3>
    <p><?php echo $post['description'];?> <br />Date <?php echo $post['date'];?><br />
    <a href="<?php echo site_url('admin/edit_post/'.$post['id'])?>">Edit</a> &nbsp; 
    <a href="<?php echo site_url('admin/delete_post/'.$post['id'])?>" onclick="return confirm('Delete this post?')">Delete</a></p>
</div>
<?php endforeach;?>