<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>My Blog</title>

    <link rel="stylesheet" href="<?php echo base_url('public/css/style.css'); ?>">
</head>
<body>


    <div id="header">
        <a href="<?php echo base_url();?>" style="color: #fff">HOME</a> &nbsp;  <a href="<?php echo site_url('admin_login');?>">Login</a>
        <?php if($this->session->userdata('admin_logged')):?>
            <a href="<?php echo site_url('admin_login/logout');?>">Logout</a>
        <?php endif;?>
        
    </div>
    <div id="container">