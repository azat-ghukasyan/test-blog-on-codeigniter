-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Июн 05 2015 г., 16:25
-- Версия сервера: 5.6.21
-- Версия PHP: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `my_blog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
`id` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `title`, `description`, `text`, `date`) VALUES
(1, 'Hello World', 'Aenean pellentesque est purus, et pharetra ligula aliquet ac. Duis feugiat viverra tincidunt. Mauris euismod leo enim, et gravida ligula hendrerit in. Sed non massa ut risus pretium congue a eget eros.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam odio quam, placerat at erat mollis, varius feugiat erat. In porttitor dapibus faucibus. Vestibulum augue tortor, volutpat eu malesuada euismod, porttitor posuere odio. Fusce ultricies vulputate mattis. Ut consectetur faucibus tellus eu placerat. Duis metus quam, efficitur a eros accumsan, sollicitudin ornare sem. Morbi tempus magna vel odio mollis auctor. Vestibulum tempus magna quis iaculis varius.</p>\r\n\r\n<p>Aenean pellentesque est purus, et pharetra ligula aliquet ac. Duis feugiat viverra tincidunt. Mauris euismod leo enim, et gravida ligula hendrerit in. Sed non massa ut risus pretium congue a eget eros. Duis feugiat scelerisque massa. Fusce vitae arcu volutpat tellus volutpat facilisis vitae sit amet tellus. Vivamus gravida consectetur nulla, vel egestas tellus tristique vel.</p>\r\n\r\n<p> </p>\r\n', '2015-06-05 13:00:07'),
(3, 'Hello', '', 'hello \r\nasdsa as d', '2015-06-05 14:19:08'),
(6, 'title', 'desc', 'aaaa', '2015-06-05 14:23:59');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `login` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `pass`) VALUES
(1, 'user', '8cb2237d0679ca88db6464eac60da96345513964');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
